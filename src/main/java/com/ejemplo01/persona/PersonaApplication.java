package com.ejemplo01.persona;

import com.ejemplo01.library.database.DatabaseModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@DatabaseModule
@SpringBootApplication

public class PersonaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonaApplication.class, args);
	}

}
