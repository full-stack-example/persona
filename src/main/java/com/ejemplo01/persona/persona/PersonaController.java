package com.ejemplo01.persona.persona;

import com.ejemplo01.library.database.jooq.tables.pojos.Persona;
import org.jooq.types.UInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping({"/personas"})
public class PersonaController {
    @Autowired
    PersonaService personaService;

    @GetMapping
        public List<Persona> listar(){
        return personaService.listar();
    }

    @PostMapping
    public Persona add(@RequestBody Persona persona){
        return personaService.add(persona);
    }

    @GetMapping(path = {"/{id}"})
    public Persona listarid(@PathVariable("id") int id ){
    return personaService.listarId(id);
    }

    @PutMapping(path = {"/{id}"})
    public void editar(@RequestBody Persona persona, @PathVariable("id") int id){
        persona.setId(UInteger.valueOf(id));
        personaService.editar(persona);
    }

    @DeleteMapping(path = {"/{id}"})
    public void delete(@PathVariable("id") int  id){
         personaService.delete(id);
    }
}
