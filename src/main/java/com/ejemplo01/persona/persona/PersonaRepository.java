package com.ejemplo01.persona.persona;


import com.ejemplo01.library.database.jooq.tables.pojos.Persona;
import org.jooq.DSLContext;
import org.jooq.types.UInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import static com.ejemplo01.library.database.jooq.Tables.PERSONA;

import java.util.List;

@Repository
public class PersonaRepository {
    @Autowired
    DSLContext dslContext;


    public List<Persona> listar() {
        List<Persona> personasList = dslContext.select(PERSONA.fields())
                .from(PERSONA)
                .fetchInto(Persona.class);

        return personasList;
    }

    public Persona listarById(int id) {
        return dslContext.select(PERSONA.fields())
                .from(PERSONA)
                .where(PERSONA.ID.eq(UInteger.valueOf(id)))
                .fetchOne()
                .into(Persona.class);
    }

    public Persona add(Persona persona) {
        return dslContext.insertInto(PERSONA,
                PERSONA.NOMBRE,
                PERSONA.APELLIDOS)
                .values(
                        persona.getNombre(),
                        persona.getApellidos())
                .returningResult(PERSONA.ID, PERSONA.NOMBRE, PERSONA.APELLIDOS)
                .fetchOne()
                .into(Persona.class);
    }

    public void edit(Persona persona) {
         dslContext.update(PERSONA)
                .set(PERSONA.NOMBRE, persona.getNombre())
                .set(PERSONA.APELLIDOS, persona.getApellidos())
                .where(PERSONA.ID.eq(persona.getId()))
                .execute();
    }


    public void delete(int id) {
      dslContext.delete(PERSONA)
                .where(PERSONA.ID.eq(UInteger.valueOf(id)))
                .execute();
    }


}
