package com.ejemplo01.persona.persona;


import com.ejemplo01.library.database.jooq.tables.pojos.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;

    public List<Persona> listar(){
    return personaRepository.listar();
    }

    public Persona add(Persona persona){
       return personaRepository.add(persona);
    }

    public Persona listarId(int id){
        return personaRepository.listarById(id);
    }

    public void editar(Persona persona){
         personaRepository.edit(persona);
    }

    public void delete(int id){
         personaRepository.delete(id);
    }
}
