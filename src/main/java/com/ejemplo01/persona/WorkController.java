package com.ejemplo01.persona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class WorkController {
    @RequestMapping("/")
    public String works(){
        return "persona works";
    }
}
